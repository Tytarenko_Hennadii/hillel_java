package Task_1;

public class Fibonacci {
    public static void main(String[] args){
        int n0 = 1, n1 = 1, n2;
        int mas[] = new int[22];
        mas[0] = n0;
        mas[1] = n1;
        for(int i = 0; i < 20; i++){
            n2=n0+n1;
            mas[i+2]=n2;
            n0=n1;
            n1=n2;
            System.out.print(mas[i] + " ");
        }
    }
}

