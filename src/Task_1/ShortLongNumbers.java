package Task_1;

public class ShortLongNumbers {

    public static void main(String[] args) {
        int mas[] = {122, 555555,55778,577,666,77};
        int s = mas[0];
        int l = mas[0];

        for(int j = 0; j< mas.length; j++) {
            if(mas[j] > l)
                l = mas[j];
            else if (mas[j] < s)
                s = mas[j];
        }
        System.out.println("Longest Number is " + l +  " with " + Count(l) + " digits");
        System.out.println("Shortest Number is " + s +  " with " + Count(s)+ " digits");
    }

    public static int Count(int c){
        int result = 0;
        while(c != 0){
            ++result;
            c /= 10;
        }
        return result;
    }

}
