package Task_2;

/**
 1. Создать класс по спецификации. Определить конструктор для всех полей. Создать отдельно класс процессор, который
 будет выполнять определенные действия с объектамии и печатать объект. В методе main создать массив объектов и используя
 класс процессор выполнить необходимые операции.

 Car: id, Марка, Модель, Год выпуска, Цвет, Цена, Регистрационный номер.
 Создать массив объектов. Вывести:
 a) список автомобилей заданной марки;
 b) список автомобилей заданной модели, которые эксплуатируются больше n лет;
 c) список автомобилей заданного года выпуска, цена которых больше указанной.
 */

public class Cars {

    int         id;
    String      car_type;
    String      model;
    int         year;
    String      color;
    int         price;
    int         reg_number;

    public Cars(int id, String car_type, String model, int year, String color, int price, int reg_number){
        this.id = id;
        this.car_type = car_type;
        this.model = model;
        this.year = year;
        this.color = color;
        this.price = price;
        this.reg_number=reg_number;
    }

    @Override
    public String toString() {
        return "Cars{" +
                "id=" + id +
                ", car_type='" + car_type + '\'' +
                ", model='" + model + '\'' +
                ", year=" + year +
                ", color='" + color + '\'' +
                ", price=" + price +
                ", reg_number=" + reg_number +
                '}';
    }
}
