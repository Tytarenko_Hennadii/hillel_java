package Task_2;

public class MainCar {
    public static void main(String[] args){
        Cars[] mass_of_cars = new Cars[3];
        mass_of_cars[0] = new Cars(1, "Mercedes", "C200", 1998,"green", 60000, 12345);
        mass_of_cars[1] = new Cars(2, "BMW", "m5", 2000,"green", 60000, 12345);
        mass_of_cars[2] = new Cars(3, "KIA", "rio", 2002, "green", 60000, 12345);

        System.out.println("===================");
        Processor.printCarsOlderYears(mass_of_cars,"rio");
        Processor.printCarsMark(mass_of_cars, "BMW");
        Processor.printCarsPrice(mass_of_cars, 2000,20000);
    }
}
