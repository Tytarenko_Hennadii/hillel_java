package Task_2;


public class Processor {

    static void printCarsOlderYears(Cars[] cars, String model){
        int Current_year = 2017;
        int n = 10;
        for (int i = 0; i < cars.length; i++){
            if ((Current_year - cars[i].year > n) && (cars[i].model == model)) {
                System.out.println(cars[i]);
            }
        }
    }

    static void printCarsMark(Cars[] cars, String car_type){
        for (int i = 0; i < cars.length; i++){
            if (cars[i].car_type == car_type) {
                System.out.println(cars[i]);
            }
        }
    }

    static void printCarsPrice(Cars[] cars, int year, int price){
        for (int i = 0; i < cars.length; i++){
            if ((cars[i].year == year)&&(cars[i].price > price)) {
                System.out.println(cars[i]);
            }
        }
    }

}
