package Task_3;

 abstract class GeometricFigure {
    private int edges;
    private String color;

    public double SquareArea(double a, double b){
        return a*b;
    }

    public String getColor(){
        return color;
    }

    public void setEdge(int edges){
        this.edges=edges;
    }

}
